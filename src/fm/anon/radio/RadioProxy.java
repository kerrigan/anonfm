package fm.anon.radio;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class RadioProxy {
	private String mHost;
	private int mRemotePort;
	private int mLocalPort;
	private boolean isRunning = false;

	public RadioProxy(String host, int remotePort, int localPort) {
		mHost = host;
		mRemotePort = remotePort;
		mLocalPort = localPort;
	}
	
	public void runServer() throws IOException{
		isRunning = true;
		
		ServerSocket ss = new ServerSocket(mLocalPort);
		
		final byte[] request = new byte[1024];
		byte[] reply = new byte[4096];
		
		while(isRunning){
			Socket client = null, server = null;
			
			try{
				client = ss.accept();
				
				final InputStream fromClient = client.getInputStream();
				final OutputStream toClient = client.getOutputStream();
				
				try{
					server = new Socket(mHost, mRemotePort);
				} catch(IOException e){
					PrintWriter out = new PrintWriter(new OutputStreamWriter(toClient));
			          out.println("Proxy server cannot connect to " + mHost + ":" +
			                      mRemotePort + ":\n" + e);
			          out.flush();
			          client.close();
			          continue;
				}
				
				final InputStream fromServer = server.getInputStream();
				final OutputStream toServer = server.getOutputStream();
				
				Thread t = new Thread(){
					public void run() {
						int bytes_read;
			            try {
			              while((bytes_read = fromClient.read(request)) != -1) {
			                toServer.write(request, 0, bytes_read);
			                toServer.flush();
			              }
			            } catch (IOException e) {}
			            
			            try {toServer.close();} catch (IOException e) {}
					};
				};
				
				t.start();
				
				int bytes_read;
		        try {
		          while((bytes_read = fromServer.read(reply)) != -1) {
		            toClient.write(reply, 0, bytes_read);
		            toClient.flush();
		          }
		        }
		        catch(IOException e) {}

		        // The server closed its connection to us, so close our 
		        // connection to our client.  This will make the other thread exit.
		        toClient.close();
				
			} catch(IOException e){
				
			}
		}
	}
}
