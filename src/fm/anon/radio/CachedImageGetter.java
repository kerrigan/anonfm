package fm.anon.radio;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.util.LruCache;
import android.text.Html.ImageGetter;

public class CachedImageGetter implements ImageGetter {
	private static LruCache<String, Drawable> mImageCache;
	private Context mContext;

	
	public CachedImageGetter(Context context) {
		mContext = context;
		if(mImageCache == null)
			mImageCache = new LruCache<String, Drawable>(4 * 1024 * 1024);
	}
	
	@Override
	public Drawable getDrawable(String source) {
		Drawable image = mImageCache.get(source);
		if(image == null){
			try {
				Bitmap bitmap = urlToBitmap("http://anon.fm" + source);
				image = new BitmapDrawable(mContext.getResources(), bitmap);
				image.setBounds(0, 0, 				bitmap.getWidth(), 				bitmap.getHeight());
				mImageCache.put(source, image);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return image;
	}
	
	private Bitmap urlToBitmap(String url) throws IOException {
		// Log.d("Bitmap URL", url);
		URL Url = new URL(url);
		URLConnection connection = Url.openConnection();
		connection.setUseCaches(false);
		connection
				.setRequestProperty(
						"User-Agent",
						"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.8; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
		connection.setConnectTimeout(15000);
		connection.setReadTimeout(15000);
		InputStream stream = connection.getInputStream();
		Bitmap bm = BitmapFactory.decodeStream(stream);
		return bm;
	}
}
