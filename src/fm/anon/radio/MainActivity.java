package fm.anon.radio;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import com.googlecode.androidannotations.annotations.AfterInject;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.EBean;
import com.googlecode.androidannotations.annotations.RootContext;
import com.googlecode.androidannotations.annotations.SeekBarProgressChange;
import com.googlecode.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;


@EActivity(R.layout.activity_main)
public class MainActivity extends Activity {

	private static MediaPlayer mPlayer;
	private CoocooAdapter mAdapter;
	
	@ViewById(R.id.cookooLog) ListView mCoocooList;
	@ViewById(R.id.write2DJ) Button mSend2DJ;
	@ViewById(R.id.toogleRadio)	ToggleButton mToggleRadio;
	@ViewById(R.id.currentTrack) TextView mTrackInfo;
	@ViewById(R.id.volumeBar) SeekBar mVolumeBar;
	@ViewById(R.id.volumeText) TextView mVolumeText;
	@ViewById(R.id.chkTb) CheckBox mTbCheckbox;
	@ViewById(R.id.chkLow) CheckBox mLowCheckbox;
	
	private static Timer mCoocooTimer;
	private static Timer mTitleTimer;
	private static Pattern mTitlePattern = Pattern.compile("<td>Current Song:</td>[\\n\\s]+<td class=\"streamdata\">(.+?)</td>");
	
	private static String RADIO_URL = "http://anon.fm:8000/radio";
	private static String RADIO_URL_LOW = "http://anon.fm:8000/radio-low";
	private static String RADIO_URL_TB = "http://anon.fm:8000/timeback";
	private static String RADIO_URL_TB_LOW = "http://anon.fm:8000/timeback-low";
	private static String CURRENT_URL = RADIO_URL;
	private static Long statusLength = 0L;
	private static String lastStatus = "";

	@AfterViews
	void init(){
		if (mPlayer == null) {
			mPlayer = new MediaPlayer();
			try {
				mPlayer.setDataSource(CURRENT_URL);
				mPlayer.prepareAsync();
				mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
					
					@Override
					public void onPrepared(MediaPlayer mp) {
						// TODO Auto-generated method stub
						mPlayer.start();	
					}
				});
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		mVolumeText.setText(mVolumeBar.getProgress() + "%");
		
		mLowCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				boolean isTb = mTbCheckbox.isChecked();
				boolean radioEnabled = mToggleRadio.isChecked();
				if(isChecked){
					if(isTb){
						CURRENT_URL = RADIO_URL_TB_LOW;
					} else {
						CURRENT_URL = RADIO_URL_LOW;
					}
				} else {
					if(isTb){
						CURRENT_URL = RADIO_URL_TB;
					} else {
						CURRENT_URL = RADIO_URL;
					}
				}
				if(radioEnabled)
					setStream(mPlayer, CURRENT_URL);
			}
		});
		
		mTbCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				boolean radioEnabled = mToggleRadio.isChecked();
				boolean isLow = mLowCheckbox.isChecked();
				if (isChecked) {
					if (isLow) {
						CURRENT_URL = RADIO_URL_TB_LOW;
					} else {
						CURRENT_URL = RADIO_URL_TB;
					}
				} else {
					if (isLow) {
						CURRENT_URL = RADIO_URL_LOW;
					} else {
						CURRENT_URL = RADIO_URL;
					}
				}
				if (radioEnabled)
					setStream(mPlayer, CURRENT_URL);
			}
		});
		
		/*
		mVolumeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				mPlayer.setVolume(progress / 100.f, progress / 100.f);
				mVolumeText.setText(mVolumeBar.getProgress() + "%");
			}
		});*/
		
		mToggleRadio.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked == false)
					mPlayer.stop();
				else {
					try {
						mPlayer.reset();
						mPlayer.setDataSource(RADIO_URL);
						mPlayer.prepareAsync();
						mPlayer.setOnPreparedListener(new OnPreparedListener() {
							
							@Override
							public void onPrepared(MediaPlayer mp) {
								// TODO Auto-generated method stub
								mp.start();	
							}
						});
					} catch (IllegalStateException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
		
		
		if (mCoocooTimer == null) {
			mCoocooTimer = new Timer();
			//mCoocooTimer.scheduleAtFixedRate(new NewsUpdater(), 0, 5000);
		}
		
		if(mTitleTimer == null){
			mTitleTimer = new Timer();
			mTitleTimer.scheduleAtFixedRate(new UpdateTitle(), 0, 5000);
		}
		
	}
	
	@SeekBarProgressChange(R.id.volumeBar)
	public void onVolumeChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		mPlayer.setVolume(progress / 100.f, progress / 100.f);
		mVolumeText.setText(mVolumeBar.getProgress() + "%");
	}
	
	
	/*
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (mPlayer == null) {
			mPlayer = new MediaPlayer();
			try {
				mPlayer.setDataSource(CURRENT_URL);
				mPlayer.prepareAsync();
				mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
					
					@Override
					public void onPrepared(MediaPlayer mp) {
						// TODO Auto-generated method stub
						mPlayer.start();	
					}
				});
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		mSend2DJ = (Button) findViewById(R.id.write2DJ);
		mToggleRadio = (ToggleButton) findViewById(R.id.toogleRadio);
		mTrackInfo = (TextView) findViewById(R.id.currentTrack);
		mVolumeBar = (SeekBar)findViewById(R.id.volumeBar);
		mVolumeText = (TextView)findViewById(R.id.volumeText);
		mTbCheckbox = (CheckBox)findViewById(R.id.chkTb);
		mLowCheckbox = (CheckBox)findViewById(R.id.chkLow);
		mCoocooList = (ListView)findViewById(R.id.cookooLog);
		mAdapter = new CoocooAdapter(this);
		mCoocooList.setAdapter(mAdapter);
		
		mVolumeText.setText(mVolumeBar.getProgress() + "%");
		
		
		mLowCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				boolean isTb = mTbCheckbox.isChecked();
				boolean radioEnabled = mToggleRadio.isChecked();
				if(isChecked){
					if(isTb){
						CURRENT_URL = RADIO_URL_TB_LOW;
					} else {
						CURRENT_URL = RADIO_URL_LOW;
					}
				} else {
					if(isTb){
						CURRENT_URL = RADIO_URL_TB;
					} else {
						CURRENT_URL = RADIO_URL;
					}
				}
				if(radioEnabled)
					setStream(mPlayer, CURRENT_URL);
			}
		});
		
		mTbCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				boolean radioEnabled = mToggleRadio.isChecked();
				boolean isLow = mLowCheckbox.isChecked();
				if (isChecked) {
					if (isLow) {
						CURRENT_URL = RADIO_URL_TB_LOW;
					} else {
						CURRENT_URL = RADIO_URL_TB;
					}
				} else {
					if (isLow) {
						CURRENT_URL = RADIO_URL_LOW;
					} else {
						CURRENT_URL = RADIO_URL;
					}
				}
				if (radioEnabled)
					setStream(mPlayer, CURRENT_URL);
			}
		});
		
		
		mVolumeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				mPlayer.setVolume(progress / 100.f, progress / 100.f);
				mVolumeText.setText(mVolumeBar.getProgress() + "%");
			}
		});
		
		mToggleRadio.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked == false)
					mPlayer.stop();
				else {
					try {
						mPlayer.reset();
						mPlayer.setDataSource(RADIO_URL);
						mPlayer.prepareAsync();
						mPlayer.setOnPreparedListener(new OnPreparedListener() {
							
							@Override
							public void onPrepared(MediaPlayer mp) {
								// TODO Auto-generated method stub
								mp.start();	
							}
						});
					} catch (IllegalStateException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
		
		mSend2DJ.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, CookooSendActivity.class);
				startActivity(intent);
			}
		});
		

		if (mCoocooTimer == null) {
			mCoocooTimer = new Timer();
			mCoocooTimer.scheduleAtFixedRate(new NewsUpdater(), 0, 5000);
		}
		
		if(mTitleTimer == null){
			mTitleTimer = new Timer();
			mTitleTimer.scheduleAtFixedRate(new UpdateTitle(), 0, 5000);
		}
	}
	*/
	
	@Click(R.id.write2DJ)
	void handleSend2DJ(){
		CookooSendActivity_.intent(this).start();
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		//new Thread(new UpdateTitle()).start();
		//new Thread(new NewsUpdater()).start();
	}


	/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}*/
	
	private void setStream(MediaPlayer mp, String stream){
		try {
			mp.reset();
			mp.setDataSource(stream);
			mp.prepareAsync();
			//mp.setOnPreparedListener(mp.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private class UpdateTitle extends TimerTask{
		
		@Override
		public void run() {
			try {
				HttpEntity response = Utils.urlToConnection("http://anon.fm:8000/status.xsl?mount=/radio");
				Long newLength = response.getContentLength();
				if(!newLength.equals(statusLength)){
					String html = Utils.streamToString(response.getContent());
					Matcher matcher = mTitlePattern.matcher(html);
					if(matcher.find()){
						lastStatus = matcher.group(1);
						runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								setTitle(lastStatus);
								mTrackInfo.setText(lastStatus);	
							}
						});
						statusLength = newLength;
					}
				} else {
					runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							setTitle(lastStatus);
							mTrackInfo.setText(lastStatus);	
						}
					});
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	
	private class UpdateNews implements Runnable {
		@Override
		public void run() {
			try {
				URL url = new URL("http://anon.fm/answers.js");
				String html = Utils.streamToString(url.openStream());
				runOnUiThread(new NewsSetter(html));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private class NewsSetter implements Runnable {
		private String mHtml;

		public NewsSetter(String html) {
			// TODO Auto-generated constructor stub
			mHtml = html;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			setNews(mHtml);
		}

	}

	private class NewsUpdater extends TimerTask {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			Log.d("ANONFM", "updated started");
			new Thread(new UpdateNews()).start();
		}

	}

	public void setNews(String json) {
		mAdapter.parseJSON(json);
	}


}
