package fm.anon.radio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Utils {
	public static String streamToString(InputStream stream) {

		InputStreamReader reader = new InputStreamReader(stream);
		BufferedReader buff = new BufferedReader(reader);
		StringBuffer strBuff = new StringBuffer();

		String s;
		try {
			while ((s = buff.readLine()) != null) {
				strBuff.append(s);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return strBuff.toString();
	}
	
	public static HttpEntity urlToConnection(String pageUrl) throws IOException {
		HttpClient client = new DefaultHttpClient();

		HttpGet request = new HttpGet(pageUrl);
		request.setHeader("User-Agent", "useragentsaregays");
		HttpResponse response = client.execute(request);
		return response.getEntity();
	}
	
	public static Bitmap urlToBitmap(String url) throws IOException {
		// Log.d("Bitmap URL", url);
		URL Url = new URL(url);
		URLConnection connection = Url.openConnection();
		connection.setUseCaches(false);
		connection
				.setRequestProperty(
						"User-Agent",
						"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.8; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
		connection.setConnectTimeout(15000);
		connection.setReadTimeout(15000);
		InputStream stream = connection.getInputStream();
		Bitmap bm = BitmapFactory.decodeStream(stream);
		return bm;
	}
}
