package fm.anon.radio;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class CoocooMessage {
	public boolean isAd = false;
	public String author = ""; // 1
	public String incoming = ""; // 2
	public String time = ""; // 3
	public String jid = ""; // 4
	public String reply = ""; // 5

	public static CoocooMessage parseJSON(JSONArray json) throws JSONException {
		CoocooMessage msg = new CoocooMessage();
		msg.author = json.getString(1);
		if (msg.author.equals("!")) {
			msg.isAd = true;
		}

		if (msg.isAd) {
			msg.author = "Объявление";
		} else {
			msg.incoming = json.getString(2);
		}
		
		msg.time = json.getString(3);
		msg.jid = json.getString(4);
		msg.reply = json.getString(5);
		return msg;
	}
}
