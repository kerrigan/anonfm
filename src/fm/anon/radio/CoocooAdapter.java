package fm.anon.radio;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CoocooAdapter extends BaseAdapter {
	private List<CoocooMessage> mMessagesList = new ArrayList<CoocooMessage>();
	private Context mContext;

	public CoocooAdapter(Context context) {
		mContext = context;
	}

	public void parseJSON(String json) {
		try {
			JSONArray messagesJS = new JSONArray(json);
			clear();
			for (int i = 0; i < messagesJS.length(); ++i) {
				add(CoocooMessage.parseJSON(messagesJS.getJSONArray(i)));
			}
			notifyDataSetChanged();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void clear() {
		clear(false);
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return mMessagesList.size();
	}

	public CoocooMessage getItem(int position) {
		// TODO Auto-generated method stub
		return mMessagesList.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	private void clear(boolean needUpdate) {
		mMessagesList.clear();
		if (needUpdate)
			notifyDataSetChanged();
	}

	private void add(CoocooMessage post) {
		mMessagesList.add(post);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		CoocooMessage msg = getItem(position);
		View v = convertView;
		if (v == null || !(v instanceof RelativeLayout)) {
			LayoutInflater vi = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.cookoo_post, null);

			ViewHolder holder = new ViewHolder();
			holder.author = (TextView) v.findViewById(R.id.txtAuthor);
			holder.time = (TextView) v.findViewById(R.id.txtTime);
			holder.incoming = (TextView) v.findViewById(R.id.txtIncoming);
			holder.reply = (TextView) v.findViewById(R.id.txtReply);
			v.setTag(holder);
		}

		ViewHolder vh = (ViewHolder) v.getTag();

		if (msg.isAd) {
			vh.incoming.setVisibility(View.GONE);
		} else {
			vh.incoming.setVisibility(View.VISIBLE);
			vh.incoming.setText(Html.fromHtml(msg.incoming));
		}

		vh.author.setText(Html.fromHtml(msg.author));
		vh.reply.setText(Html.fromHtml(msg.reply,
				null,
				//new CachedImageGetter(mContext),
				null));
		vh.time.setText(Html.fromHtml(msg.time));
		return v;
	}

	private class ViewHolder {
		TextView author;
		TextView time;
		TextView incoming;
		TextView reply;
	}

}
