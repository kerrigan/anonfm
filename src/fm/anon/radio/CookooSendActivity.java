package fm.anon.radio;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.googlecode.androidannotations.annotations.AfterInject;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

@EActivity(R.layout.activity_cookoo_send)
public class CookooSendActivity extends Activity {

	public String mCapcthaId = "";
	private Pattern mCaptchaPattern = Pattern
			.compile("<img src=\"/feedback/(\\d+)\\.gif");
	
	
	@ViewById(R.id.coocooText)
	EditText mCoocooText;
	
	@ViewById(R.id.btnSend)
	Button mSendButton;
	
	@ViewById(R.id.imgCaptcha)
	ImageView mCaptchaImage;
	
	@ViewById(R.id.txtCaptcha)
	TextView mCaptchaText;

	
	
	@AfterViews
	void init(){
		setTitle("Написать в диджейку");
		new Thread(new GetCaptcha()).start();
		
	}
	
	
	@Click(R.id.btnSend)
	void handleSend(){
		new Thread(new SendCoocoo(mCaptchaText.getText().toString(),
				mCoocooText.getText().toString())).start();
	}
	
	@Click(R.id.imgCaptcha)
	void handleCaptcha(){
		new Thread(new GetCaptcha()).start();
	}
	
	/*
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cookoo_send);

		setTitle("Написать в диджейку");

		mSendButton = (Button) findViewById(R.id.btnSend);
		mCoocooText = (EditText) findViewById(R.id.coocooText);
		mSendButton.setEnabled(false);
		mCaptchaImage = (ImageView) findViewById(R.id.imgCaptcha);
		mCaptchaText = (TextView) findViewById(R.id.txtCaptcha);

		new Thread(new GetCaptcha()).start();

		mCoocooText
				.setOnEditorActionListener(new EditText.OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						// TODO Auto-generated method stub
						return false;
					}
				});

		mCaptchaImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new Thread(new GetCaptcha()).start();
			}
		});

		mSendButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new Thread(new SendCoocoo(mCaptchaText.getText().toString(),
						mCoocooText.getText().toString())).start();
			}
		});
	}
	*/

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_cookoo_send, menu);
		return true;
	}

	private class GetCaptcha implements Runnable {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				HttpEntity response = Utils
						.urlToConnection("http://anon.fm/feedback/");
				String html = Utils.streamToString(response.getContent());
				Matcher matcher = mCaptchaPattern.matcher(html);
				if (matcher.find()) {
					mCapcthaId = matcher.group(1);
					final Bitmap captcha = Utils
							.urlToBitmap("http://anon.fm/feedback/"
									+ mCapcthaId + "/.gif");
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							mCaptchaImage.setImageBitmap(captcha);
							mSendButton.setEnabled(true);
						}
					});
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						mSendButton.setEnabled(false);
					}
				});
				e.printStackTrace();
			}
		}

	}

	private class SendCoocoo implements Runnable {
		String mText;
		String mCaptcha;

		public SendCoocoo(String captcha, String text) {
			mText = text;
			mCaptcha = captcha;
		}

		@Override
		public void run() {
			// send message
			HttpClient client = new DefaultHttpClient();

			HttpPost request = new HttpPost("http://anon.fm/feedback/");
			try {
				List<BasicNameValuePair> paramArrays = Arrays.asList(
						new BasicNameValuePair("cid", mCapcthaId),// номер капчи
						new BasicNameValuePair("msg", mText),
						new BasicNameValuePair("check", mCaptcha));
				// Текст
				request.setEntity(new UrlEncodedFormEntity(paramArrays, "UTF-8"));
				request.setHeader("User-Agent", "useragentsaregays");
				HttpResponse response = client.execute(request);

				String html = Utils.streamToString(response.getEntity()
						.getContent());
				if (html.indexOf("Неверный код подтверждения") == -1) {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							Toast.makeText(getApplicationContext(),
									"Сообщение отправлено", Toast.LENGTH_SHORT)
									.show();
						}
					});
				} else {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							Toast.makeText(getApplicationContext(),
									"Неверная капча", Toast.LENGTH_SHORT)
									.show();
						}
					});
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
